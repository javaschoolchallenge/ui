import React from 'react';
import Dashboard from "./components/dashboard/Dashboard";
import Provider from "react-redux/es/components/Provider";
import {globalStore} from "./store/globalStore";

function App() {
  return (
      <Provider store={globalStore}>
        <Dashboard/>
      </Provider>
  );
}

export default App;

import {DEFAULT_CAPACITY} from "../store/stories/reducer";
import avatarAnalyst from "../logo/analyst.jpg";
import avatarDev1 from "../logo/dev1.jpg";
import avatarDev2 from "../logo/dev2.jpg";
import avatarDev3 from "../logo/dev3.jpg";
import avatarDev5 from "../logo/dev5.jpg";
import avatarDev4 from "../logo/dev4.jpg";
import avatarDev6 from "../logo/dev6.jpg";
import avatarDev7 from "../logo/dev7.jpg";
import avatarDev8 from "../logo/dev8.jpg";
import avatarDev9 from "../logo/dev9.jpg";
import avatarDev10 from "../logo/dev10.jpg";
import avatarTest1 from "../logo/test1.jpg";
import avatarTest2 from "../logo/test2.jpg";
import avatarTest3 from "../logo/test3.jpg";
import avatarTest4 from "../logo/test4.jpg";
import avatarTest5 from "../logo/test5.jpg";
import avatarTest6 from "../logo/test6.jpg";
import avatarTest7 from "../logo/test7.jpg";
import avatarTest8 from "../logo/test8.jpg";
import avatarTest9 from "../logo/test9.jpg";
import avatarTest10 from "../logo/test10.jpg";

export function mapStory(beStory) {
    return {
        id: beStory.id,
        name: "Story " + beStory.id,
        status: beStory.status,
        estimation: beStory.estimation,
        priority: beStory.priority,
        assigned: beStory.assigned
    }
}

export function getOpenStories(stories) {
    return stories.filter(story => story.status === "OPEN");
}

export function getInProgressStories(stories) {
    return stories.filter(story => story.status === "IN_PROGRESS");
}

export function getResolvedStories(stories) {
    return stories.filter(story => story.status === "RESOLVED");
}

export function getTestStories(stories) {
    return stories.filter(story => story.status === "IN_TEST");
}

export function getClosedStories(stories) {
    return stories.filter(story => story.status === "CLOSED");
}

export function calculateCapacity(stories) {
    let result = DEFAULT_CAPACITY;
    stories.forEach(story => result = result - story.estimation);
    return result > 0 ? result : 0;
}

export function getAvatar(story) {
    return story.status === "OPEN" ? avatarAnalyst
        : story.status === "IN_PROGRESS" ? (story.assigned.name === "Dev1" ? avatarDev1
            : story.assigned.name === "Dev2" ? avatarDev2
                : story.assigned.name === "Dev3" ? avatarDev3
                    : story.assigned.name === "Dev5" ? avatarDev5
                        : story.assigned.name === "Dev4" ? avatarDev4
                            : story.assigned.name === "Dev6" ? avatarDev6
                                : story.assigned.name === "Dev7" ? avatarDev7
                                    : story.assigned.name === "Dev8" ? avatarDev8
                                        : story.assigned.name === "Dev9" ? avatarDev9
                                            : story.assigned.name === "Dev10" ? avatarDev10 : '')
            : story.status === "IN_TEST" ? (story.assigned.name === "Test1" ? avatarTest1
                : story.assigned.name === "Test2" ? avatarTest2
                    : story.assigned.name === "Test3" ? avatarTest3
                        : story.assigned.name === "Test4" ? avatarTest4
                            : story.assigned.name === "Test5" ? avatarTest5
                                : story.assigned.name === "Test6" ? avatarTest6
                                    : story.assigned.name === "Test7" ? avatarTest7
                                        : story.assigned.name === "Test8" ? avatarTest8
                                            : story.assigned.name === "Test9" ? avatarTest9
                                                : story.assigned.name === "Test10" ? avatarTest10 : '') : ''
}


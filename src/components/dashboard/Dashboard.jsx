import * as React from "react";
import DashboardGroupButton from "./DashboardGroupButton";
import StoryList from "./StoryList";
import Header from "./Header";
import connect from "react-redux/es/connect/connect";
import {checkStarted, createStory, endSprint, fetchStories, startSprint} from "../../store/stories/action";

class Dashboard extends React.Component{
    render() {
        return (
            <div>
                <Header/>
                <div style={{margin: 30}}>
                    <DashboardGroupButton
                        createStory={this.props.createStory}
                        fetchStories={this.props.fetchStories}
                        stories={this.props.stories}
                        endSprint={this.props.endSprint}
                        startSprint={this.props.startSprint}
                        checkStarted={this.props.checkStarted}
                        isSprintStarted={this.props.isSprintStarted}
                        capacity={this.props.capacity}
                    />
                    <StoryList
                        stories={this.props.stories}
                        capacity={this.props.capacity}
                        isSprintStarted={this.props.isSprintStarted}
                    />
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({
    stories: state.story.stories,
    capacity: state.story.capacity,
    isSprintStarted: state.story.isSprintStarted,
});

const mapDispatchToProps = {
    createStory: createStory,
    fetchStories: fetchStories,
    endSprint: endSprint,
    startSprint: startSprint,
    checkStarted: checkStarted
};

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
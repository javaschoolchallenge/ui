import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import avatar from "../../logo/bordeaux-avatar.jpg";
import * as React from "react";
import Typography from "@material-ui/core/Typography";
import makeStyles from "@material-ui/core/styles/makeStyles";
import Avatar from "@material-ui/core/Avatar";


const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
    },
    avatar: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));

export default function Header() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Avatar alt="Bordeaux Avatar" src={avatar} className={classes.avatar}/>
                    <Typography variant="h6" className={classes.title}>
                        Bordeaux Dashboard
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}
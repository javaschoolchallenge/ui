import * as React from "react";
import Grid from "@material-ui/core/Grid";
import {
    calculateCapacity,
    getClosedStories,
    getInProgressStories,
    getOpenStories,
    getResolvedStories,
    getTestStories
} from "../../util/StoryUtil";
import {StoryCard} from "./StoryCard";
import {CapacityCard} from "./CapacityCard";

export default class StoryList extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            stories: [
                {data: [], status: "OPEN", id: 1},
                {data: [], status: "IN PROGRESS", id: 2},
                {data: [], status: "RESOLVED", id: 3},
                {data: [], status: "TEST", id: 4},
                {data: [], status: "DONE", id: 5}
            ],
            capacity: props.capacity
        };
    }

    componentDidUpdate(prevProps) {
        const {isSprintStarted, capacity, stories} = this.props;
        let prevOpen = getOpenStories(prevProps.stories),
            open = getOpenStories(stories),
            prevInProgress = getInProgressStories(prevProps.stories),
            inProgress = getInProgressStories(stories),
            prevResolved = getResolvedStories(prevProps.stories),
            resolved = getResolvedStories(stories),
            prevTest = getTestStories(prevProps.stories),
            test = getTestStories(stories),
            preDone = getClosedStories(stories),
            done = getClosedStories(stories);

        if (prevOpen.length !== open.length
            || prevInProgress.length !== inProgress.length
            || prevResolved.length !== resolved.length
            || prevTest.length !== test.length
            || preDone.length !== done.length
            || prevProps.capacity !== capacity
        ) {
            this.updateDashboard()
        }

        if (isSprintStarted && prevProps.capacity !== capacity) {
            this.setState({capacity: calculateCapacity(stories)})
        }
    }

    updateDashboard = () => {
        const {stories, capacity} = this.props;

        this.setState({
            stories: [
                {data: getOpenStories(stories), status: "OPEN", id: 1},
                {data: getInProgressStories(stories), status: "IN PROGRESS", id: 2},
                {data: getResolvedStories(stories), status: "RESOLVED", id: 3},
                {data: getTestStories(stories), status: "TEST", id: 4},
                {data: getClosedStories(stories), status: "DONE", id: 5}
            ],
            capacity: capacity > 0 ? capacity : 0
        })
    };

    render() {
        let {stories, capacity} = this.state;
        stories = stories.sort((a,b) => a.id - b.id);

        return (
            <div>
                <Grid container spacing={1}>
                    {
                        stories.map(item =>
                            <Grid item xs={2} key={item.id}>
                                {StoryCard(item.data, item.status)}
                            </Grid>
                        )
                    }
                    <Grid item xs={2}>
                        {CapacityCard(capacity, stories)}
                    </Grid>
                </Grid>
            </div>
        )
    }
}
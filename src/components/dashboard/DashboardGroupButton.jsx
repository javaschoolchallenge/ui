import * as React from "react";
import Button from "@material-ui/core/Button";
import {getInProgressStories, getOpenStories, getResolvedStories, getTestStories} from "../../util/StoryUtil";

const buttonStyle = {
    marginRight: 10,
    marginTop: 10,
    marginBottom: 20
};

export default class DashboardGroupButton extends React.Component {

    componentDidMount() {
        this.props.fetchStories();
        this.props.checkStarted();
    }

    componentDidUpdate() {
        const {stories, isSprintStarted, fetchStories} = this.props;

        let open = getOpenStories(stories),
            inProgress = getInProgressStories(stories),
            resolved = getResolvedStories(stories),
            test = getTestStories(stories),
            sprintFinished = open.length === 0
                && inProgress.length === 0
                && resolved.length === 0
                && test.length === 0;

        if (isSprintStarted && !sprintFinished) {
            clearInterval(this.interval);
            this.interval = setInterval(fetchStories, 1000);
        } else {
            clearInterval(this.interval);
        }
    }

    onAddStoryClick = () => {
        this.props.createStory();
    };

    onEndSprintClick = () => {
        this.props.endSprint();
    };

    onStartSprintClick = () => {
        this.props.startSprint();
    };

    render() {
        const {capacity, isSprintStarted, stories} = this.props;
        let disableAddStoryButton = capacity < 0 || isSprintStarted,
            disableStartSprintButton = isSprintStarted || stories.length === 0;

        return (
            <div>
                <h1>Sprint 1 </h1>
                <Button variant="contained" color="primary" style={buttonStyle} onClick={this.onAddStoryClick} disabled={disableAddStoryButton}>
                    Add story
                </Button>
                <Button variant="contained" color="primary" style={buttonStyle} onClick={this.onStartSprintClick} disabled={disableStartSprintButton}>
                    Start sprint
                </Button>
                <Button variant="contained" color="secondary" style={buttonStyle} onClick={this.onEndSprintClick}>
                    End sprint
                </Button>
            </div>
        );
    }
}
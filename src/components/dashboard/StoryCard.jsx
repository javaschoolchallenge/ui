import CardHeader from "@material-ui/core/CardHeader";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import Avatar from "@material-ui/core/Avatar";
import ListItemText from "@material-ui/core/ListItemText";
import Card from "@material-ui/core/Card";
import * as React from "react";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import {getAvatar} from "../../util/StoryUtil";

export const cardStyle = {
    height: '60vh',
};

export const headerStyle = {
    backgroundColor: "#C0C0C0", color: "white"
};

export const StoryCard = (data, status) => {
    data = data.sort((a,b) => a.priority - b.priority);

    return (
        <Card style={cardStyle}>
            <CardHeader title={status} style={headerStyle}/>
            <Divider/>
            <List dense component="div" role="list" style={{maxHeight: '50vh', overflow: 'auto'}}>
                {data.map(value => {
                    return (
                        <div key={value.id}>
                            <ListItem key={value.id} alignItems="flex-start">
                                <ListItemAvatar>
                                    <Avatar alt="Bordeaux Avatar" src={getAvatar(value)}/>
                                </ListItemAvatar>
                                <ListItemText id={value.id}
                                              primary={<div
                                                  style={{fontStyle: "italic", fontWeight: "bold"}}>{value.name}</div>}
                                              secondary={"Priority: " + value.priority}
                                />
                                <ListItemSecondaryAction>
                                    <Avatar variant="rounded" style={{backgroundColor: "#228B22"}}>
                                        {value.estimation}
                                    </Avatar>
                                </ListItemSecondaryAction>
                            </ListItem>
                            <Divider variant="inset" component="li"/>
                        </div>
                    );
                })}
            </List>
        </Card>
    )
};
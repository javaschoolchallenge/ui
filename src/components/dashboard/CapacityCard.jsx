import CardHeader from "@material-ui/core/CardHeader";
import Divider from "@material-ui/core/Divider";
import Card from "@material-ui/core/Card";
import * as React from "react";
import Typography from "@material-ui/core/Typography";
import {cardStyle, headerStyle} from "./StoryCard";

const capacityStyle = {
    textAlign: 'center',
    marginTop: 50
};

const capacityTitleStyle = {
    textAlign: 'center',
    marginTop: 10
};

export const CapacityCard = (capacity) => {
    return (
        <Card style={cardStyle}>
            <CardHeader title={"TEAM CAPACITY"} style={headerStyle}/>
            <Divider/>
            <Typography variant="h1" style={capacityStyle}>
                {capacity}
            </Typography>
            <Typography variant="h3" style={capacityTitleStyle}>
                story points
            </Typography>
        </Card>
    )
};
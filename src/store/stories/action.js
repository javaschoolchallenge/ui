import * as axios from "axios";
import {calculateCapacity, mapStory} from "../../util/StoryUtil";

export const DASHBOARD_URL = process.env.DASHBOARD_URL ? process.env.DASHBOARD_URL : "/dashboard";
export const ANALYST_URL = process.env.ANALYST_URL ? process.env.ANALYST_URL + "/story" : "/analyst/story";

console.log(process.env);

export const ADD_STORY = "ADD_STORY";
export const FETCH_STORIES = "FETCH_STORIES";
export const CLEAR_STORIES = "CLEAR_STORIES";
export const SET_SPRINT_PROGRESS = "SET_SPRINT_PROGRESS";
export const SET_CAPACITY = "SET_CAPACITY";

export function addStoryToSprint(story) {
    return {
        type: ADD_STORY,
        payload: story
    }
}

export function getStories(stories) {
    return {
        type: FETCH_STORIES,
        payload: stories
    }
}

export function clearStories() {
    return {
        type: CLEAR_STORIES,
        payload: []
    }
}

export function setSprintProgress(value) {
    return {
        type:  SET_SPRINT_PROGRESS,
        payload: value
    }
}

export function setCapacity(value) {
    return {
        type:  SET_CAPACITY,
        payload: value
    }
}

export function createStory() {
    return (dispatch) => {
        axios.post(ANALYST_URL)
            .then((response) => {
                dispatch(addStoryToSprint(mapStory(response.data)));
            }).catch(error => console.log(error))
    }
}

export function fetchStories() {
    return (dispatch) => {
        axios.get(DASHBOARD_URL + "/story")
            .then((response) => {
                console.log("Stories was retrieved");
                let stories = response.data.map(story => mapStory(story));
                dispatch(getStories(stories));
                dispatch(setCapacity(calculateCapacity(stories)));
            }).catch(error => console.log(error))
    }
}

export function endSprint() {
    return (dispatch) =>
        axios.post(DASHBOARD_URL + "/sprint/stop")
            .then(response => {
                dispatch(clearStories())
            }).catch(error => console.log(error))
}

export function startSprint() {
    return (dispatch) =>
        axios.post(DASHBOARD_URL + "/sprint/start")
            .then(response => {
                dispatch(setSprintProgress(true))
            }).catch(error => console.log(error))
}

export function checkStarted(){
    return (dispatch) => {
        axios.get(DASHBOARD_URL + "/sprint")
            .then(response => {dispatch(setSprintProgress(response.data))})
            .catch(error => console.log(error))
    }
}
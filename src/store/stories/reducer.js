import {ADD_STORY, CLEAR_STORIES, FETCH_STORIES, SET_CAPACITY, SET_SPRINT_PROGRESS} from "./action";

export const DEFAULT_CAPACITY = 200;

const initialState = {
    stories: [],
    isSprintStarted: false,
    capacity: DEFAULT_CAPACITY
};

export default function storyReducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_STORIES:
            return {
                ...state,
                stories: action.payload
            };
        case CLEAR_STORIES:
            return initialState;
        case ADD_STORY:
            return {
                ...state,
                stories: [
                    ...state.stories,
                    action.payload
                ],
                capacity: state.capacity - action.payload.estimation
            };
        case SET_SPRINT_PROGRESS:
            return {
                ...state,
                isSprintStarted: action.payload,
               // capacity: action.payload === true ? 0 : DEFAULT_CAPACITY
            };
        case SET_CAPACITY:
            return {
                ...state,
                capacity: action.payload
            };
        default:
            return state;
    }
}
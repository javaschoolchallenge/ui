import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {rootReducer} from "./reducer";
import { composeWithDevTools } from 'redux-devtools-extension';

export function configureStore() {
    const middleware = [thunk];
    const middlewareEnhancer = applyMiddleware(...middleware);
    return createStore(rootReducer, composeWithDevTools(middlewareEnhancer));
}

export const globalStore = configureStore();
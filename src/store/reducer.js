import {combineReducers} from "redux";
import storyReducer from "./stories/reducer";

export const rootReducer = combineReducers({
    story: storyReducer
});